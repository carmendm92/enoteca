<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\WineBarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('homepage');

Route::get('/inserisci-enoteca', [WineBarController::class, 'create'])->name('wineBar.create');

Route::post('/crea-enoteca', [WineBarController::class, 'store'])->name('wineBar.store');

Route::get('/dettaglio-enoteca/{wineBar}', [PublicController::class, 'show'])->name('wineBar.detail');

Route::get('/modifica-birreria/{wineBar}',[WineBarController::class, 'edit'])->name('wineBar.edit');

Route::put('/edita-birreria/{wineBar}',[WineBarController::class, 'update'])->name('wineBar.update');

Route::get('/le-mie-enoteche', [WineBarController::class, 'wineBarUser'])->name('wineBar.user');

Route::get('/ricerca-enoteche', [PublicController::class, 'SearchwineBar'])->name('wineBar.search');

Route::delete('/elimina-birreria/{wineBar}',[WineBarController::class, 'destroy'])->name('wineBar.delete');

Route::delete('/utente/{user}', [WineBarController::class, 'deleteUser'])->name('user.delete');