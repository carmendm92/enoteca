<?php

namespace App\Http\Controllers;

use App\Models\wineBar;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(){

        $wineBars = wineBar::with('user')->orderBy('created_at', 'DESC')->take(5)->get();

        return view('welcome', compact ('wineBars'));
    
       }    

    public function SearchwineBar(){

        $wineBars = wineBar::with('user')->orderBy('created_at', 'DESC')->get();
        
        return view('wineBar.searchRegion');
    }


    public function show(wineBar $wineBar)
    {  
        return view('wineBar.show', compact('wineBar'));
    }
}