<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Place;
use App\Models\wineBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WineBarController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $places = Place::all();
        return view('wineBar.create', compact('places'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $wineBar = Auth::user()->wineBars()->create([
            'name'=>$request->name,
            'description'=>$request->description,
            'img'=>$request->file('img')->store('public/img')
        ]);
        $id = $request->place;
        $place = Place::where('id', $id)->first();
        $wineBar->place()->associate($place);
        $wineBar->save();
        return redirect(route('homepage'))->with('message', 'La tua birreria è stata inserita');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\wineBar  $wineBar
     * @return \Illuminate\Http\Response
     */
    public function edit(wineBar $wineBar)
    {
        return view('wineBar.edit', compact('wineBar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\wineBar  $wineBar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wineBar $wineBar)
    {
        $wineBar->update([
        'name'=>$request->name,
        'description'=>$request->description,
        'img'=>$request->file('img')->store('public/img')
        ]);

        return redirect (route('wineBar.detail', compact('wineBar')))->with('message', 'Enoteca modificata');
    }

    public function wineBarUser(){
        $wineBars = Auth::user()->wineBars()->get();
        return view('wineBar.wineBarUser', compact('wineBars'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\wineBar  $wineBar
     * @return \Illuminate\Http\Response
     */
    public function destroy(wineBar $wineBar)
    {
        $wineBar->delete();
        return redirect (route('homepage'))->with('destroy', 'Enoteca eliminata');
    }

    public function deleteUser(User $user){
        foreach ($user->wineBars as $wineBar){
            $wineBar->user()->dissociate();
            $wineBar->user()->associate(2);
            $wineBar->save();
        }
        $user->delete();

        return redirect (route('homepage', $user))->with('status', 'il tuo profilo è stato eliminato');
    }
}

