<?php

namespace App\Models;

use App\Models\wineBar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Place;

class Place extends Model
{
    use HasFactory;
    protected $fillable =[
        'name'
    ];

    public function wineBars(){
        return $this->hasMany(wineBar::class, 'place_id');
    }
}