<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 mt-3">
                <h1 class="text-center">
                    LOGIN
                </h1>
            </div>
            <div class="col-12 col-md-12 my-3 d-flex justify-content-center">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{route('login')}}">
                    @csrf
    
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label fw-bolder">Email</label>
                      <input type="email" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label fw-bolder">Password</label>
                      <input type="password" class="form-control " id="exampleInputPassword1" name="password">
                    </div>
                    <button type="submit" class="btn btn-success">Login</button>
                  </form>
            </div>
        </div>
    </div>
        
    </x-layout>