<x-layout>
    <div class="container">
        <div class="row justify-content-center align-items-center mt-">
            <div class="col-12 col-md-6 text-center mt-5">
                <h1 class="fw-bolder">Registarti</h1>
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        </div>
        <div class="row justify-content-center align-items-center mt-5">
            <div class="col-12 col-md-4">
                <form method="POST" action="{{route('register')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label fw-bolder">Inserisci il tuo nome</label>
                        <input type="text" class="form-control" name="name">
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label fw-bolder">Inserisci la tua Email</label>
                      <input type="email" class="form-control" name="email">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label fw-bolder">Inserisci la Password</label>
                      <input type="password" class="form-control" name="password">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label fw-bolder">Conferma la Password</label>
                        <input type="password" class="form-control" name="password_confirmation">
                      </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-success mb-4">Registrati</button>
                    </div>
                  </form>
                  
            </div>
        </div>
    </div>
    </x-layout>