<x-layout>
    <div class="container">
        <div class="row">

                {{-- messaggio di inserimento --}}
            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif 
                {{-- messaggio di eliminazione --}}
            @if(session('destroy'))
                <div class="alert alert-danger">
                    {{ session('destroy') }}
                </div>
            @endif
        </div>


        <div class="row my-5 justify-content-center">
            <div class="col-12">
                <h1>Le tue Enoteche</h1>
            </div>
            @foreach ($wineBars as $wineBar)
            <div class="col-12 col-md-6 col-xl-3 mt-3">
                <x-card
                nome="{{$wineBar->name}}"
                luogo="{{$wineBar->place->name}}"
                imagine="{{Storage::url($wineBar->img)}}"
                autore="{{$wineBar->user->name}}"
                route="{{route('wineBar.detail', compact('wineBar'))}}"
                ></x-card>
            </div>
             @endforeach
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-end">
                @if (Auth::id()==$wineBar->user->id)
                <form action="{{route('user.delete', ['user'=>Auth::user()])}})}}" method="POST">
                @csrf
                @method("delete")
                    <div class="mt-4">
                        <button type="submit" class="btn border border-dark bg-danger fw-bolder mb-4">Elimina Profilo</button>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</x-layout>