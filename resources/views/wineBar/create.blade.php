<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 text-center m-4 ">
                <h1 class="fw-bolder">Inserisci la tua Enoteca</h1> 
            </div>
            @if ($errors->any()) 
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <form method="POST" action="{{route('wineBar.store')}}"enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputtext1" class="form-label fw-bolder">Inserisci il titolo</label>
                      <input type="text" class="form-control border-dark" name="name">
                    </div>
                     <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">Inserisci descrizione</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}</textarea>
                      </div>
                      <div>
                        <p class="fw-bolder">Seleziona una regione</p>
                          <select name="place" class=" text-center m-3"> 
                            @foreach ($places as $place)
                                <option  value="{{$place->id}}">{{$place->name}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label fw-bolder">inserisci immagine</label>
                        <input type="file" class="form-control border-dark" id="exampleInputEmail1" name="img">
                     </div>
                    <div class="mb-4 text-center ">
                        <button type="submit" class="btn btn-success">Invia</button>
                    </div>
                    <div class="mb-4 text-center ">
                        <a href="{{route('homepage')}}" class="btn btn-danger">Annulla</a>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</x-layout>