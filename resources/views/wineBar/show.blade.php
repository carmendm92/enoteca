<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 p-4">
                <h1 class="fw-bolder text-center">{{$wineBar->name}}</h1>
                <h4 class="card-title text-center">{{$wineBar->place->name}}</h4>
            </div>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>

        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6 mt-5 p-4 border border-info">
                <p class="card-text ">{!! nl2br(e($wineBar->description)) !!}</p> 
                <div class="col-12 text-center mb-2">
                    <p class="card-text text-end"><em>{{$wineBar->user->name}}</em></p>
                </div>
            </div>
            <div class="col-12 col-md-4 m-5 p-4 border border-warning ">
                <img src="{{Storage::url($wineBar->img)}}" class="img-fluid" alt="{{$wineBar->title}}">
            </div>
            <div class="col-12 d-flex justify-content-center align-items-center flex-column">
                @if (Auth::id()==$wineBar->user->id)
                <a class="btn border border-dark button_card bg-warning mt-3" href="{{route('wineBar.edit', compact('wineBar'))}}">Modifica</a>
                    <form action="{{route('wineBar.delete', compact ('wineBar'))}}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger border border-dark mt-3">Elimina</button>
                    </form>
                @endif
            </div>
            <div class="col-12 d-flex justify-content-end my-2">
                <a class="btn border border-dark button_card bg-success text-end" href="{{route('homepage')}}">Indietro</a>
            </div>
        </div> 
    </div>
</x-layout>