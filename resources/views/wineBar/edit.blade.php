<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 text-center m-4 ">
                <h1 class="fw-bolder">Modifica la tua enoteca</h1> 
            </div>
            @if ($errors->any()) 
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <form method="POST" action="{{route('wineBar.update', compact('wineBar'))}}"enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                      <label for="exampleInputtext1" class="form-label fw-bolder">Modifica il titolo</label>
                      <input type="text" class="form-control border-dark" name="name" value="{{$wineBar->name}}">
                    </div>
                     <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">Modifica descrizione</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}{{$wineBar->description}}</textarea>
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label fw-bolder">La tua immagine</label>
                        <img src="{{Storage::url($wineBar->img)}}" alt="{{$wineBar->name}}" class="m-2"width="200" height="200">
                      </div>
                      <div class="mb-3">
                        <p class="fw-bolder">Inserisci una nuovaimmagine</p>
                        <input type="file" class="form-control border-dark" id="exampleInputEmail1" name="img">
                     </div>
                    <div class="mb-4 text-center ">
                        <button type="submit" class="btn btn-success">Invia</button>
                    </div>
                    <div class="mb-4 text-center ">
                        <a href="{{route('wineBar.detail', compact('wineBar'))}}" class="btn btn-danger">Annulla</a>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</x-layout>