<x-layout>
    <div class="container">
        <div class="row">

                {{-- messaggio di inserimento --}}
            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif 
                {{-- messaggio di eliminazione --}}
            @if(session('destroy'))
                <div class="alert alert-danger">
                    {{ session('destroy') }}
                </div>
            @endif
        </div>


        <div class="row justify-content-center">
            <div class="col-12 col-md-4 mt-5 p-3">
                <p>
                    <strong>Enoteche 2.0</strong> nasce per raccogliere le enoteche del nostre paese in un unico portale. Le migliori 
                    enoteche qui verranno descritte nei dettagli, i vini che potete trovare, gallerie fotografiche e un calendario con tutti
                    gli eventi. Registra anche tu la tua enoteca e fatti trovare.
                </p>
            </div>
            <div class="col-12 col-md-8 mt-3 p-5">
                <div class="img"></div>
            </div>
        </div>

        <div class="row my-5 justify-content-center">
            <div class="col-12">
                <h1>Enoteche Recenti</h1>
            </div>
            @foreach ($wineBars as $wineBar)
            <div class="col-12 col-md-6 col-xl-3 mt-3">
                <x-card
                nome="{{$wineBar->name}}"
                luogo="{{$wineBar->place->name}}"
                imagine="{{Storage::url($wineBar->img)}}"
                autore="{{$wineBar->user->name}}"
                route="{{route('wineBar.detail', compact('wineBar'))}}"
                ></x-card>
            </div>
             @endforeach
        </div>
    </div>
</x-layout>