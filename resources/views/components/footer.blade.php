<footer class="container-fluid footer d-flex align-items-center justify-content-center">
    <div class="row">
        <div class="col-12 ">
            <div class="container-icon d-flex justify-content-center">
                <a class="nav-link " href="#">
                    <i class="fa-brands fa-facebook-f link-footer fa-2x"></i>
                </a>
                <a class="nav-link " href="#">
                    <i class="fa-brands fa-twitter link-footer fa-2x"></i>
                </a>
                <a class="nav-link " href="#">
                    <i class="fa-brands fa-pinterest-p link-footer fa-2x"></i>
                </a>
                <a class="nav-link" href="#">
                    <i class="fa-brands fa-instagram link-footer fa-2x"></i>
                </a>
            </div>
        </div>
        <div class="col-12 text-center mt-3">
            <a class=" fw-bolder text-white " href="mailto:carmendm92@gmail.com">carmendm92@gmail.com</a>
        </div>
    </div>
</footer>