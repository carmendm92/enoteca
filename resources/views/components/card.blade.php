    <div class="d-flex justify-content-center align-items-center flex-column" style="width: 18rem;">
        <div class="card">
            <h2 class="card-header text-center">
                {{$nome}}
            </h2>
        </div>
        <h4 class="text-center p-2">{{$luogo}}</h4>
        <img class="p-4 text-center img-fluid"src="{{$imagine ?? ''}}" alt="{{$nome}}" widith="200px" height="200px">
        <h5 class="text-center p-2">Inserita da <br><em>{{$autore}}</em></h5>
        <a class="btn border border-dark button_card bg-success" href="{{$route ?? ''}} ">Leggi altro</a>
  </div>
</div>