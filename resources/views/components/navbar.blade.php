<nav class="navbar navbar-expand-lg sticky-top navbar-dark navbar_wine ">
    <div class="container-fluid p-2">
      <a class="navbar-brand" href="/"><i class="fa-solid fa-wine-bottle logo_wine logo_wine_initial"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto">
          <li class="nav-item">
            <a class="nav-link active link-navbar" aria-current="page" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('wineBar.search')}}">Cerca enoteca</a>
          </li>
          <li class="nav-item">
          @guest 
            <a class="nav-link active" aria-current="page" href="{{route('login')}}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('register')}}">Registrati</a>
          </li>
          @endguest
    
          @auth
            <li class="nav-item">
              <a class="nav-link" href="{{route('wineBar.create')}}">Inserisci Enoteca</a>
            </li> 
           
        </ul>
        <div class="container-pannel-user" id="navbarSupportedContent">
          <ul class="navbar-nav m-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-wine-glass-empty text-warning"></i> BenBevuto {{Auth::user()->name}}
              </a>
              <ul class="dropdown-menu navbar_wine text-white" aria-labelledby="navbarDropdown">
                <li>
                  <a class="dropdown-item uppercase navbar_wine " href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a>
                </li>
                  <form action="{{route('logout')}}" method="POST" id="form-logout">
                    @csrf
                  </form>
                  <li>
                    <a class="dropdown-item uppercase navbar_wine" href="{{route('wineBar.user')}}">Le mie enoteche</a>
                  </li>
              </ul>
            </li>
          </ul>
        @endauth
      </div>
    </div>
  </div>
</nav>