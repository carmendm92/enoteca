window.onscroll = function (event){

    // funzione backgraund nav bar
    let navbar = document.querySelector(".navbar_wine")
    if (document.documentElement.scrollTop > 50){
        navbar.classList.add("border-shadow")
    }else{
        navbar.classList.remove("border-shadow")
    }
    
    // funzione colore logo navbar
    let logo = document.querySelector(".logo_wine")
    if (document.documentElement.scrollTop > 50){
        logo.classList.add("color_wine")
    }else{
        logo.classList.remove("color_wine")
    }
}